import { Component } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid'; // useful for typechecking


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'mycalendar';

  calendarPlugins = [dayGridPlugin];
  
}
