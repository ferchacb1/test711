using System.Collections.Generic;
using System.Threading.Events;
using testApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace testApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class EventsController : ControllerBase
    {
       private readonly DatabaseContext _context; 

       public EventsController(DatabaseContext context)
       {
           _context = context;
       }
       
       [HttpGet] //https://localhost:5001/api/event/1 
       public async Task<ActionResult<IEnumerable<Event>>> GetEvents()
       {
           return await _context.Events.ToListAsync();
       }

       [HttpGet("{id}")] //https://localhost:5001/api/event/1
       public async Task<ActionResult<Event>> GetEvent(long id)
       {
           var event = await _context.Events.FindAsync(id);

           if(event == null){
               return NotFound();
           }
           return event;
       }

       [HttpPost] //https://localhost:5001/api/event/
       public async Task<ActionResult<Event>> PostEvent(Event event)
       {
            _context.Events.Add(event);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEvent", new { id = event.Id}, event);
       }

       [HttpDelete("{id}")]
       public async Task<ActionResult<Event>> DeleteEvent(long id){
           var event = await _context.Events.FindAsync(id);

           if(event == null){
               return NotFound();
           }
           _context.Events.Remove(event);
           await _context.SaveChangesAsync();
           return event;
       }

        [HttpPut("{id}")]
       public async Task<ActionResult<Event>> UpdateEvent(long id, Event event){
           if(id != event.Id){
               return BadRequest();
           }
           _context.Entry(event).State = EntityState.Modified;
           await _context.SaveChangesAsync();
           return CreatedAtAction("GetEvent", new {id = event.Id}, event);
       }
    }
}