namespace testApi.Models
{
    public class Event
    {
        public long id {get; set;}

        public string descri {get; set;}

        public string startDate {get; set;}
        
        public string endDate {get; set;}

        public string startHour {get; set;}

        public string endHour {get; set;}
    }
}