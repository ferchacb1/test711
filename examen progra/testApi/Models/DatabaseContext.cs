using Microsoft.EntityFrameworkCore;

namespace testApi.Models
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            
        }

        //tabla de eventos
        public DbSet<Event> Events {get; set;}

    }
}